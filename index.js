const cors = require('cors')
const express = require('express');
const Server = require('./app/server');
const SimpleCache = require('./app/simple-cache');
const axios = require('axios');
let exprs = express();
exprs.use(cors());
let app = new Server(exprs, new SimpleCache(60*10), axios, 'px82gfqiyad7t8q2lrgc5r1ynk5gngulhlszzhxu');
app.listen(80);
