/**
 * Simple request cache.
 */
class SimpleCache {
    /**
     *
     * @param ttl time in seconds entry remains in cache
     */
    constructor(ttl) {
        this.ttl = ttl;
        this.entries = {};
        this.cleanupWorker();
    }

    set(key, content) {
        this.entries[key] = {ts: this.unxTimestamp(), content: content};
    }

    unxTimestamp() {
        return Date.now() / 1000;
    }

    /**
     * Attempts to load key from cache, returns false if data not found
     * @param key
     * @returns {boolean|*}
     */
    get(key) {
        if (key in this.entries) {
            console.log('Cache hit: ' + key);
            return this.entries[key].content;
        }
        console.log('Cache miss: ' + key);
        return false;
    }

    cleanup() {
        let now = this.unxTimestamp();
        Object.keys(this.entries).forEach(key => {
            if (now - this.entries[key].ts > this.ttl) {
                console.log('Cache Expire ' + key);
                delete this.entries[key];
            }
        });
    }

    /**
     * Periodically cleans up cache entries
     */
    cleanupWorker() {
        setInterval(() => {
            this.cleanup();
        }, 60 * 1000);
    }
}

module.exports = SimpleCache;
