// https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Ftechcrunch.com%2Ffeed%2F&api_key=px82gfqiyad7t8q2lrgc5r1ynk5gngulhlszzhxu
const rss2jsUrl = 'https://api.rss2json.com/v1/api.json';

class Server {
    constructor(express, cache, axios, apiKey) {
        this.express = express;
        this.simpleCache = cache;
        this.axios = axios;
        this.apiKey = apiKey;
    }

    async requestHandler(req, res) {
        let url = req.query.url;
        let data = this.simpleCache.get(url);
        if (data === false) {
            try {
                let resp = await this.axios.get(rss2jsUrl, {params: {rss_url: url, api_key: this.apiKey}});
                data = resp.data;
                this.simpleCache.set(url, data);
            } catch (err) {
                console.log('Unable to fetch ' + url);
                console.log(err);
            }
        }
        if (data) {
            res.json(data);
        } else {
            res.status(500).send('Something  nasty happened');
        }
    }

    listen(port) {
        this.express.get('/', this.requestHandler.bind(this));
        this.express.listen(port, () => {
            console.log(`Example app listening at http://localhost:${port}`)
        })
    }


}

module.exports = Server;
