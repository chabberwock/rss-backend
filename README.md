# Backend for RSS Reader

## About

This app uses rss2json.com to parse rss feeds and return them to client. It also adds basic caching functionality

## Installation

Clone app from git and run `npm install`

## Runnning

* As node app: `npm start`
* As Docker container: 
```
$ docker build -t rss-backend .
$ docker run -it --rm --name rss-app rss-backend
```

## Testing

App uses mochajs.org testing framework

````
$ npm test

````
