const assert = require('assert');
const simple = require('simple-mock');
const Server = require('../app/server');
const SimpleCache = require('../app/simple-cache');
const express = require('express');
const cors = require("cors");
const apiKey = 'px82gfqiyad7t8q2lrgc5r1ynk5gngulhlszzhxu';

function mockReq(url) {
    return {query: {url: url}};
}

function mockRes() {
    let res = {};
    simple.mock(res, 'json');
    simple.mock(res, 'status').callbackWith(null, 'send');
    return res;
}

describe('Server', function() {
    describe('requestHandler(req, res)', function() {

        it('should check cache on request', async function() {
            let testValue = 'testme';
            let exprs = express();
            let axios = require('axios');
            //exprs.use(cors());
            let cache = new SimpleCache(60*10);
            simple.mock(cache, 'get').returnWith(testValue);
            let srv = new Server(exprs, cache, axios, apiKey);
            await srv.requestHandler(mockReq('http://example.com'), mockRes());
            assert.equal(cache.get.lastCall.returned, testValue);
        });

        it('should return cached value if exists', async function() {
            let testValue = 'testme';
            let exprs = express();
            let axios = require('axios');
            //exprs.use(cors());
            let cache = new SimpleCache(60*10);
            simple.mock(cache, 'get').returnWith(testValue);
            let srv = new Server(exprs, cache, axios, apiKey);
            let res = mockRes();
            await srv.requestHandler(mockReq('http://example.com'), res);
            assert.equal(res.json.lastCall.arg, testValue);
        });

        it('should attempt to load url if not cached', async function() {
            let testValue = 'testme';
            let exprs = express();
            //exprs.use(cors());
            let cache = new SimpleCache(60*10);

            let axios = require('axios');
            simple.mock(axios, 'get').resolveWith({data: testValue});
            let srv = new Server(exprs, cache, axios, apiKey);
            let res = mockRes();
            await srv.requestHandler(mockReq('http://example.com'), res);
            assert.equal(res.json.lastCall.arg, testValue);
        });

        //todo: bad url handling, SimpleCache coverage



    });
});
